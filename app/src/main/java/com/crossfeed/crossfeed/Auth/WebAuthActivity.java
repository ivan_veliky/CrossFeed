package com.crossfeed.crossfeed.Auth;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.crossfeed.crossfeed.MainActivity;
import com.crossfeed.crossfeed.R;

/**
 * Created by hawk on 19.11.2016.
 */
public class WebAuthActivity extends AppCompatActivity {


     public void onCreate(Bundle savedInstanceState) {
         super.onCreate(savedInstanceState);
         setContentView(R.layout.activity_web_auth);

         String url="";
         String NetName = "";
         Bundle bundle = getIntent().getExtras();

         if(bundle != null) {
             url = bundle.getString("url");
             NetName = bundle.getString("netName");
         }

         WebView webView = (WebView)findViewById(R.id.web_window);
         ProgressBar progressBar = (ProgressBar)findViewById(R.id.progressBar);
         MyWebViewClient webViewClient = new MyWebViewClient(this, progressBar, NetName);
         webView.setWebViewClient(webViewClient);
         webView.loadUrl(url);
     }
}

  class MyWebViewClient extends WebViewClient {

      SharedPreferences prefs;
      SharedPreferences.Editor editor;
      Context context;
      ProgressBar progressBar;
      String NetName;
      Bundle bundle;

      public MyWebViewClient(Context context, ProgressBar progressBar, String NetName) {
          prefs = MainActivity.getContext().getSharedPreferences("auth_init", 0);
          this.context = context;
          this.progressBar = progressBar;
          this.NetName = NetName;
          bundle = new Bundle();
          progressBar.setVisibility(View.VISIBLE);
      }

      @Override
      public boolean shouldOverrideUrlLoading(WebView view, String url)  {
          if(!url.split("#")[0].equals("https://oauth.vk.com/blank.html"))
            view.loadUrl(url);
          else {
              editor = prefs.edit();
              String temp_token = url.split("#")[1].split("=")[1].split("&")[0];
              editor.putString(NetName+"Token", temp_token);

              if(prefs.getInt("TokenState", 0) == 0)
                  editor.putInt("TokenState", 1);

              editor.putBoolean(NetName+"AuthState", true);
              int count_net = prefs.getInt("apis_count", 0);
              count_net = count_net+1;
              editor.putInt("apis_count", count_net);
              editor.apply();
              Intent  intent = new Intent(context, AuthActivity.class);

              context.startActivity(intent);

          }
          return false;
      }

      @Override
      public void onPageFinished(WebView view, String url) {
          super.onPageFinished(view, url);
          progressBar.setVisibility(View.GONE);
      }

}
