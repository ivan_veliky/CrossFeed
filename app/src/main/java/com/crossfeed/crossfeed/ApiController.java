package com.crossfeed.crossfeed;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by hawk on 01.01.2017.
 */
public class ApiController {
    public ApiController() {}

    public static Api getApiByName(String name) {
        Api api= null;
        switch(name) {
            case "VK":
                api = new VKApi();
                break;
        }
        return api;
    }
    public static boolean isNetworkConnected() {
        Context context = MainActivity.getContext();
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo!=null && networkInfo.isConnectedOrConnecting();
    }
}
