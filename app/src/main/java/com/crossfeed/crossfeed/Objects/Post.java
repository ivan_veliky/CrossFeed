package com.crossfeed.crossfeed.Objects;

import android.content.Context;

import com.crossfeed.crossfeed.MainActivity;
import com.crossfeed.crossfeed.R;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by hawk on 30.11.2016.
 */
public class Post implements Serializable {
    private String post_text, parent_name, cropped_text;
    private String parent_icon;
    private String like_add_request_url;
    private String like_delete_request_url;
    private ArrayList<Photo> post_images;
    private long time_stamp, views;
    private long owner_id, post_id;
    private int count_likes, count_comments;
    private boolean is_liked, can_comment;

    public Post() {}

    public Post(String post_text, String parent_name) {
        this.post_text = post_text;
        this.parent_name = parent_name;
    }
    public void setPostText(String text) {
        post_text = text;
    }
    public String getPostText() {
        return post_text;
    }

    public void setParentName(String name) {
        parent_name = name;
    }
    public String getParentName() {
        return parent_name;
    }

    public void setParentIcon(String icon) {
        parent_icon = icon;
    }
    public String getParentIcon() {
        return parent_icon;
    }

    public void setPostImages(ArrayList<Photo> images) {
        post_images = images;
    }
    public ArrayList<Photo> getPostImages() {
        return post_images;
    }

    public void setTimeStamp(long time) {
        time_stamp = time;
    }
    public long getTimeStamp() {
        return time_stamp;
    }

    public void setCroppedText(String text) {
        cropped_text = text;
    }
    public String getCroppedText() {
        return cropped_text;
    }

    public void setOwnerId(long id) {
        owner_id = id;
    }
    public long getOwnerId(){
        return owner_id;
    }

    public  void setPostId(long id) {
        post_id = id;
    }
    public long getPostId() {
        return post_id;
    }

    public void setIsLiked (boolean state) {
        is_liked = state;
    }
    public boolean getIsLiked() {
        return is_liked;
    }

    public void setCountLikes(int likes) {
        count_likes = likes;
    }
    public int getCountLikes() {
        return count_likes;
    }

    public void setAddLikeRequestUrl(String url) {
        like_add_request_url = url;
    }
    public String getAddLikeRequestUrl() {
        return like_add_request_url;
    }

    public void setDeleteLikeRequestUrl(String url) {
        like_delete_request_url = url;
    }
    public String getDeleteLikeRequestUrl() {
        return like_delete_request_url;
    }

    public int getCountComments() {
        return count_comments;
    }
    public void setCountComments(int count_comments) {
        this.count_comments = count_comments;
    }

    public boolean isCanComment() {
        return can_comment;
    }
    public void setCanComment(boolean can_comment) {
        this.can_comment = can_comment;
    }

    public long getViews() {
        return views;
    }
    public void setViews(long views) {
        this.views = views;
    }

    public int getMaxWidth () {
        Context context = MainActivity.getContext();
        return (int)(MainActivity.getDisplayWidth() -
                context.getResources().getDimension(R.dimen.post_margin)*2);
    }
}
