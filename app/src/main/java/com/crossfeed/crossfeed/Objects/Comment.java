package com.crossfeed.crossfeed.Objects;

import java.io.Serializable;

/**
 * Created by CatASS on 24.03.2017.
 */

public class Comment implements Serializable {
    /**
     * Items */
    private int comment_id, likes;
    private long from_id, date;
    private String text;
    private boolean is_liked;

    /**
     * Author */
    private long author_id;
    private String author_name, author_screen_name;
    //TODO: Add author pic




    public Comment(){}

    public Comment(String text, String author_name){
        this.text = text;
        this.author_name = author_name;

        // Что я делаю и не знаю зачем
        // Это вроде конструктор?
    }

    // Ну дальше понятно
    public void setCommentId(int id){comment_id = id;}
    public int getCommentId(){return comment_id;}

    public void setCommentFromId(long author_id){from_id = author_id;}
    public long getCommentFromId(){return from_id;}

    public void setCommentTimeStamp(long time){date = time;}
    public long getCommentTimeStamp(){return date;}

    public void setCommentText(String comment_text){text = comment_text;}
    public String getCommentText(){return text;}

    public void setCommentCountLikes(int count_likes){likes = count_likes;}
    public int getCommentCountLikes(){return likes;}

    public void setIsLiked(boolean state){is_liked = state;}
    public boolean getIsLiked(){return is_liked;}

    public void setAuthorId(long id){author_id = id;}
    public long getAuthorId(){return author_id;}

    public void setAuthorName(String name){author_name = name;}
    public String getAuthorName(){return author_name;}

    public void setAuthorScreenName(String name){author_screen_name = name;}
    public String getAuthorScreenName(){return author_screen_name;}


}
