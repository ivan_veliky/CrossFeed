package com.crossfeed.crossfeed;

/**
 * Created by hawk on 15.11.2016.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.crossfeed.crossfeed.Objects.Comment;
import com.crossfeed.crossfeed.Objects.Dialog;
import com.crossfeed.crossfeed.Objects.LongPollServer;
import com.crossfeed.crossfeed.Objects.Message;
import com.crossfeed.crossfeed.Objects.Photo;
import com.crossfeed.crossfeed.Objects.PollUpdate;
import com.crossfeed.crossfeed.Objects.Post;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class VKApi extends Api {

    public static boolean did_auth;
    private String auth_url, client_id,
            request_url, scope, redirect_uri, NetName, secret,  start_from,
            webclient_id, audio_scope, web_token;

    SharedPreferences prefs;
    SharedPreferences.Editor editor;

    public VKApi() {
        super();
        auth_url = "https://oauth.vk.com/authorize?";
        client_id = "5729844";
        secret = "u3HQJiXCOoP0BQQVjrCX";
        request_url = "https://api.vk.com/method/";
        scope = "friends,wall,messages,offline";
        redirect_uri = "https://oauth.vk.com/blank.html";
        NetName = "VK";
        webclient_id = "5842506";
        audio_scope = "audio,offline";
        web_token = "";
        setPrimaryFields(auth_url, client_id, request_url, scope, redirect_uri, NetName, secret);
    }

    public void setStartFrom(String next) {
        this.start_from = next;
    }

    public String getStartFrom() {
        return this.start_from;
    }

    @Override
    public String getEncodedAuthUrl() {
        return auth_url + "client_id=" + client_id + "&redirect_id="
                + redirect_uri + "&display=" + display + "&scope=" + scope + "&response_type=" + response_type
                + "&v=" + v;
    }

    @Override
    public String getEncodedMessagesRequestUrl(int out, int offset) {
        return getRequestUrl() + "messages.get?out=" + out + "&count=" + offset +
                "&access_token=" + getAccessToken() + "&v=5.60";
    }

    public String getEncodedAuthForAudioRequest() {
        return auth_url + "client_id=" + webclient_id + "&redirect_id="
                + redirect_uri + "&display=" + display + "&scope=" + audio_scope + "&response_type=" + response_type
                + "&v=" + v;
    }

    @Override
    public ArrayList<String> getMessages() {
        ArrayList<String> messages = new ArrayList<>();
        try {
            String temp_response;
            temp_response = getResponseText(getEncodedMessagesRequestUrl(0, 10));

            JSONObject jsonObject = new JSONObject(temp_response);
            JSONObject json_response = jsonObject.getJSONObject("response");
            JSONArray json_items = json_response.getJSONArray("items");

            for (int i = json_items.length() - 1; i >= 0; i--) {
                messages.add(json_items.getJSONObject(i).get("body").toString());
            }
        } catch (JSONException e) {
            messages.add("| error. failed to parse json");
        }
        return messages;
    }

    @Override
    public String getEncodedFeedRequestUrl(int count, int return_banned,
                                           ArrayList<String> filters, boolean next_loading_flag) {

        String request_str =  getRequestUrl() + "newsfeed.get?return_banned=" + return_banned + "&count=" + count +
               "&access_token=" + getAccessToken() + "&v=5.63";

        if (getStartFrom()!= null && next_loading_flag)
            request_str += "&start_from=" + getStartFrom();

        if (filters != null) {
            request_str += "&filters=";
            for (int i = 0; i < filters.size(); i++) {
                request_str += filters.get(i);
                request_str +=  i!=filters.size()-1? ",": "";
            }
        }
        return request_str;
    }

    @Override
    public ArrayList<Post> parseFeedJson(JSONObject url_resp) {
            Post post;
            ArrayList<Post> posts = new ArrayList<>();
            Context context_main = MainActivity.getContext();
            long date;


            try {
                 JSONObject response = url_resp.getJSONObject("response");
                 setStartFrom(response.getString("next_from"));
                 JSONArray items = response.getJSONArray("items");
                    for(int i=0;i<items.length();i++) {  // looking through json items
                        post = new Post("", "");
                        JSONObject item = items.getJSONObject(i);
                        String type = item.getString("type");

                        long owner_id = item.getInt("source_id");
                        long item_id = item.getInt("post_id");

                        post.setOwnerId(owner_id);
                        post.setPostId(item_id);
                        post.setParentName(getGroupName(item, response,"source_id"));
                        post.setParentIcon(getGroupIcon(item, response));
                        post.setPostImages(getPostImages(item));
                        post.setAddLikeRequestUrl(getEncodedLikeRequest(type, owner_id, item_id, true));
                        post.setDeleteLikeRequestUrl(getEncodedLikeRequest(type, owner_id, item_id, false));

                        JSONObject likes = item.getJSONObject("likes");
                        post.setCountLikes(likes.getInt("count"));
                        post.setIsLiked(likes.getInt("user_likes")!=0);

                        JSONObject comments = item.getJSONObject("comments");
                        post.setCountComments(comments.getInt("count"));
                        post.setCanComment(comments.getInt("can_post")!=0);

                        JSONObject views = item.getJSONObject("views");
                        post.setViews(views.getLong("count"));

                        if("post".equals(type)) {
                            String post_text = item.getString("text");

                            if(post.getPostImages().size() > 1) {
                                post_text = post.getPostImages().size()+" "+
                                        context_main.getString(R.string.more_pics_ref_string);
                            }

                            String cropped_text = post_text;
                            if(post_text.length()>250)
                                cropped_text = post_text.substring(0, 250)+"..."+
                                        context_main.getString(R.string.more_text_ref_string);

                            post.setPostText(post_text);
                            post.setCroppedText(cropped_text);
                        }

                        post.setTimeStamp(Long.parseLong(item.getLong("date")+"000"));
                        posts.add(post);

                    }
            }
            catch(JSONException e) {
                post = new Post("JsonException", "Exception");
                posts.add(post);
            }
            return posts;
    }

    public String getGroupName(JSONObject item, JSONObject response, String id_name) {
        String parent_name = "";
        JSONObject parent;
        JSONArray parents;
        int parent_id;
        try {
            int source_id = item.getInt(id_name);
            // "source_id" - feed
            // "from_id" - comments

            if (source_id < 0) {
                parents = response.getJSONArray("groups");
                source_id *= -1;
            } else
                parents = response.getJSONArray("profiles");

            if (parents != null)
                for (int i = 0; i < parents.length(); i++) {
                    parent = parents.getJSONObject(i);
                    parent_id = parent.getInt("id");
                    if (parent_id == source_id) {
                        String name = parent.isNull("name") ? "" : parent.getString("name");
                        if (!"".equals(name))
                            parent_name = parent.getString("name");
                        else
                            parent_name = parent.getString("first_name") + " " + parent.getString("last_name");
                        break;
                    }
                }
        } catch (JSONException e) {
            parent_name = "GetGroupException";
        }
        if (parent_name.length() > 30)
            parent_name = parent_name.substring(0, 30) + "...";

        return parent_name;
    }

    public String getGroupIcon(JSONObject item, JSONObject response) {
        String parent_icon = null;
        JSONArray parents;
        JSONObject parent;
        int parent_id;

        try {
            int source_id = item.getInt("source_id");
            if(source_id<0) {
                parents = response.getJSONArray("groups");
                source_id*= -1;
            }
            else
                parents = response.getJSONArray("profiles");

            if(parents!=null)
                for(int i=0;i<parents.length();i++) {
                    parent = parents.getJSONObject(i);
                    parent_id = parent.getInt("id");
                    if (parent_id == source_id)
                        if (parent.get("photo_50")!=null) {
                            parent_icon = parent.getString("photo_50");
                            break;
                        }
                        else
                            parent_icon = not_found_pic_url;
                }

        }
        catch(JSONException e) {
            parent_icon = not_found_pic_url;
        }
        return parent_icon;
    }

    public ArrayList<Photo> getPostImages(JSONObject item) {
        ArrayList<Photo> post_images = new ArrayList<>();

        try {
                JSONArray attachments = item.isNull("attachments")? null: item.getJSONArray("attachments");
                if (attachments != null)
                    for (int i = 0; i < attachments.length(); i++) {
                        JSONObject attach = attachments.getJSONObject(i);
                        String attach_type = attach.get("type").toString();
                            if ("photo".equals(attach_type)) {
                                JSONObject photoJson = attach.getJSONObject("photo");
                                String pic604_url = photoJson.get("photo_604").toString();
                                int width = photoJson.isNull("width")? 0: photoJson.getInt("width");
                                int height = photoJson.isNull("height")? 0: photoJson.getInt("height");
                                Photo photo_item = new Photo(width, height, pic604_url);
                                post_images.add(photo_item);
                            }
                    }


        }
        catch(JSONException e) {
            Photo error_photo = new Photo();
            error_photo.setPhoto_604(not_found_pic_url);
            post_images.add(error_photo);
        }
        return post_images;
    }

    public String getEncodedDialogsRequestUrl(long start_id) {
        return "https://api.vk.com/method/messages.getDialogs?start_message_id="+start_id+
                "&preview_length=35&access_token="+getAccessToken();
    }

    public String getUsersRequestUrl(List<Long> ids) {
        String users_ids = "";
        if(ids!=null) {
            users_ids = "" + ids.get(0);
            for (int i = 1; i < ids.size(); i++) {
                users_ids += ",";
                users_ids += ids.get(i);
            }
        }
            return "https://api.vk.com/method/users.get?fields=photo_50&user_ids=" + users_ids+"&access_token="+
                    getAccessToken();
        //return "https://api.vk.com/method/messages.getDialogs?access_token="+getAccessToken();
    }

    public List<Dialog> parseDialogsGetJson(JSONObject data) {
        List<Dialog> dialogs = new ArrayList<>();
        Dialog dialog;

        try{
            JSONArray response  = data.getJSONArray("response");
            for(int i=1;i<response.length();i++) {
                dialog = new Dialog();
                JSONObject item = response.getJSONObject(i);
                long uid = item.getLong("uid");

                String body = item.getString("body");
                if(body.contains("<br>")) {
                    body = body.replaceAll("<br>", "\n");
                }

                String title = item.getString("title");
                boolean out = item.getInt("out")==1;
                boolean read_state = item.getInt("read_state")==0;
                boolean isGroupChat = !item.isNull("chat_id");
                long mid = item.getLong("mid");
                long date = item.getLong("date");
                long chat_id = isGroupChat? item.getLong("chat_id"):-1;
                dialog.setId(uid);
                dialog.setBody(body);
                dialog.setTitle(title);
                dialog.setIsGroupChat(isGroupChat);
                dialog.setDate(date);
                dialog.setChatId(chat_id);
                dialog.setMId(mid);
                String photo = item.isNull("photo_50")? "https://wcc723.gitbooks.io/google_design_translate/content/images/usability/usability_bidirectionality_guidelines_when12.png"
                        :item.getString("photo_50");
                dialog.setPhoto_50(photo);
                dialog.setOut(out);
                dialog.setRead_state(read_state);
                dialogs.add(dialog);
            }
        }
        catch(JSONException e) {
            Toast.makeText(MainActivity.getContext(), "error while parsing dialog json", Toast.LENGTH_SHORT).show();
        }

        return dialogs;
    }

    public List<Dialog> parseUsersJson(JSONObject data, List<Dialog> dialogs) {
        try {
            JSONArray response = data.getJSONArray("response");
            for(int i=0, j=0;i<dialogs.size();i++)  {
                Dialog dialog = dialogs.get(i);

                if(dialog.getIsGroupChat())
                    continue;

                JSONObject item = response.getJSONObject(j);
                String name = item.getString("first_name");
                name += " " + item.getString("last_name");
                dialog.setTitle(name);
                String photo = item.getString("photo_50");
                dialog.setPhoto_50(photo);
                j++;

            }
        }
        catch(JSONException e) {
            Toast.makeText(MainActivity.getContext(), "error while parsing users json", Toast.LENGTH_SHORT).show();
        }
        return dialogs;
    }

    public void setPrefsProfileInfo(JSONObject data, SharedPreferences prefs) {
        try {
            JSONArray response = data.getJSONArray("response");
            JSONObject item = response.getJSONObject(0);
            String profile_pic = item.getString("photo_50");
            SharedPreferences.Editor edit = prefs.edit();
            edit.putString("profile_pic", profile_pic);
            edit.apply();
        }
        catch(JSONException e) {
            Toast.makeText(MainActivity.getContext(), "error while parsing user profile json", Toast.LENGTH_SHORT).show();
        }

    }

    public String getEncodedHistoryRequest(long user_id, int count, long start_message_id, int rev) {
        return "https://api.vk.com/method/messages.getHistory?user_id="+user_id+"&count="+count+
                "&start_message_id="+start_message_id+"&rev="+rev+"&access_token="+getAccessToken();
    }

    public String getConnectLongPollServerRequest() {
        return "https://api.vk.com/method/messages.getLongPollServer?access_token="+
                getAccessToken();
    }

    public String getLongPollServerRequest(String key, String server, long ts, int wait) {
        return "https://"+server+"?act=a_check&key="+key+"&ts="+ts+"&wait="+wait;
    }

    public ArrayList<PollUpdate> parseLongPollResponse(JSONObject data, LongPollServer server) {
        ArrayList<PollUpdate> pollUpdates;
        try {
            server.setTs(data.getLong("ts"));
            pollUpdates = new ArrayList<>();
            PollUpdate pollUpdate;
            JSONArray updates = data.getJSONArray("updates");
            for(int i=0; i<updates.length();i++) {
                JSONArray item = updates.getJSONArray(i);
                int type = (int)item.get(0);
                if(type==4) {
                     pollUpdate = new PollUpdate();
                     pollUpdate.setMid(item.getLong(1));
                     pollUpdate.setUid(item.getLong(3));
                     pollUpdate.setDate(item.getLong(4));
                     pollUpdate.setTitle(item.getString(5));
                     pollUpdate.setText((item.getString(6)));

                    pollUpdates.add(pollUpdate);
                }
            }
        }
        catch(JSONException e) {
            Toast.makeText(MainActivity.getContext(), "error while parsing poll response json", Toast.LENGTH_SHORT).show();
            pollUpdates=null;
        }
        return pollUpdates;
    }

    public LongPollServer parseConnectLongPollServerResponse(JSONObject data) {
        LongPollServer longPollServer;
        try {
            longPollServer = new LongPollServer();
            JSONObject response = data.getJSONObject("response");
            longPollServer.setKey(response.getString("key"));
            longPollServer.setServer(response.getString("server"));
            longPollServer.setTs(response.getLong("ts"));
        }
        catch(JSONException e) {
            Toast.makeText(MainActivity.getContext(), "error while parsing connectPollServer json", Toast.LENGTH_SHORT).show();
            longPollServer=null;
        }
        return longPollServer;
    }

    public Stack<Message> parseChatHistory(JSONObject data) {
        Stack<Message> message_list = new Stack<>();
        try{
            JSONArray response = data.getJSONArray("response");

            for(int i=1;i<response.length();i++) {
                Message message = new Message();
                JSONObject item = response.getJSONObject(i);

                String body = item.getString("body");
                if(body.contains("<br>")) {
                    body = body.replaceAll("<br>", "\n");
                }
                message.setBody(body);
                message.setMid(item.getLong("mid"));
                message.setDate(item.getLong("date"));
                message.setOut(item.getInt("out")==0);
                message.setRead(item.getInt("read_state")==0);

                message_list.add(message);
            }
        }
        catch(JSONException e) {
            Toast.makeText(MainActivity.getContext(), "error while parsing history", Toast.LENGTH_SHORT).show();
        }

        return message_list;
    }

    public String getEncodedSendMessageQuery(long user_id, String message) {
        String text = message.replaceAll(" ","+");
        return "https://api.vk.com/method/messages.send?user_id="+user_id+
                "&message="+text+"&access_token="+getAccessToken();
    }

    public String getEncodedMarkAsReadRequest(ArrayList<Long> mids) {
        String start_mid="";
        for(int i=0;i<mids.size();i++) {
            start_mid += mids.get(i);
            if(i!=mids.size()-1)
                start_mid+=",";
        }
        return "https://api.vk.com/method/messages.markAsRead?message_ids="+
                start_mid+"&access_token="+getAccessToken();
    }

    public String getEncodedCommentRequest(long owner_id, long post_id, int offset,
                                           int count) {
        return "https://api.vk.com/method/wall.getComments?owner_id=" + owner_id + "&post_id=" +
                post_id + "&need_likes=1&offset=" + offset + "&count=" + count +
                "&sort=asc&extended=1&access_token=" + getAccessToken() + "&v=5.63";
    }

    public ArrayList<Comment> parseCommentsJson(JSONObject resp) {

        Comment comment;
        ArrayList<Comment> comments = new ArrayList<>();

        try {
            JSONObject response = resp.getJSONObject("response");
            JSONArray items = response.getJSONArray("items");
            if (items == null) {
                Comment empty = new Comment("Comments empty", " ");
                comments.add(empty);
                return comments;
            } else {

                for (int i = 0; i < items.length(); i++) {
                    comment = new Comment("", "");
                    JSONObject item = items.getJSONObject(i);
                    System.out.println(i);

                    comment.setCommentText(item.getString("text"));
                    comment.setCommentTimeStamp(Long.parseLong(item.getLong("date") + "000"));

                    JSONObject likes = item.getJSONObject("likes");
                    comment.setCommentCountLikes(likes.getInt("count"));
                    boolean liked_c = likes.getInt("user_likes")!=0;
                    comment.setIsLiked(liked_c);

                    comment.setAuthorName(getGroupName(item, response,"from_id"));
                    //TODO: Set author pic

                    comments.add(comment);
                }
            }
        } catch (JSONException e) {
            Comment exception = new Comment("JsonException", "Exception");
            comments.add(exception);
        }
        return comments;
    }


    public String getEncodedLikeRequest(String type, long owner_id, long item_id, boolean is_like) {
        if (is_like)
            return "https://api.vk.com/method/likes.add?type=" + type + "&owner_id=" + owner_id +
                    "&item_id=" + item_id + "&access_token=" + getAccessToken();
        else
            return "https://api.vk.com/method/likes.delete?type=" + type + "&owner_id=" + owner_id +
                    "&item_id=" + item_id + "&access_token=" + getAccessToken();
    }

    public int parseLikeRequestResponse(JSONObject data) {
        int likes = -1;
        try {
            JSONObject response = data.getJSONObject("response");
            likes = response.getInt("likes");
        } catch (JSONException e) {
            Toast.makeText(MainActivity.getContext(), "Runtime error: parsing likes failed", Toast.LENGTH_LONG).show();
        }
        return likes;
    }

/*
    public ArrayList<Post> getNewsFeed(int count, int return_banned,
                                       ArrayList<String> filters, int start_from) {

        ArrayList<Post> posts = new ArrayList<>();
        String url_str = getEncodedFeedRequestUrl(count, return_banned, filters, start_from);
        String url_resp = getResponseText(url_str);
        try{
        posts = ParseFeedJson(new JSONObject(url_resp));
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        return posts;
    }
*/
}