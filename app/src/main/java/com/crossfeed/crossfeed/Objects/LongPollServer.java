package com.crossfeed.crossfeed.Objects;

import java.io.Serializable;

/**
 * Created by hawk on 17.03.2017.
 */
public class LongPollServer implements Serializable {
    String key, server;
    long ts;

    public void setKey(String k) {
        key = k;
    }
    public void setServer(String s) {
        server = s;
    }
    public void setTs(long time) {
        ts= time;
    }

    public String getKey() {
        return key;
    }
    public String getServer() {
        return server;
    }
    public long getTs() {
        return ts;
    }
}
