package com.crossfeed.crossfeed.Objects;

import java.io.Serializable;

/**
 * Created by hawk on 26.02.2017.
 */
public class Dialog implements Serializable {
    String title, body, photo_50;
    long id,mid, date, chat_id;
    boolean out, read_state, isGroupChat;

    public void setTitle(String name) {
        title = name;
    }
    public void setBody(String text) {
        body = text;
    }
    public void setPhoto_50(String pic) {
        photo_50 = pic;
    }
    public void setId(long id) {
        this.id = id;
    }
    public void setOut(boolean flag) {
        out = flag;
    }
    public void setRead_state(boolean flag) {
        read_state = flag;
    }

    public String getTitle() {
       return title;
    }
    public String getBody() {
        return body;
    }
    public String getPhoto_50() {
        return photo_50;
    }
    public long getId() {
        return id;
    }
    public boolean getOut() {
        return out;
    }
    public boolean getRead_State()  {
        return read_state;
    }
    public boolean getIsGroupChat() {
        return isGroupChat;
    }
    public void setIsGroupChat(boolean isGroup) {
        isGroupChat = isGroup;
    }
    public long getMId() {
        return mid;
    }
    public void setMId(long id) {
        mid = id;
    }
    public void setDate(long time) {
        date = time;
    }
    public long getDate() {
        return date;
    }
    public void setChatId(long id) {
        chat_id = id;
    }
    public long getChatId() {
        return chat_id;
    }

}
