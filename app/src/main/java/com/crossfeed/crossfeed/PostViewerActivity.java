package com.crossfeed.crossfeed;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.DateUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.crossfeed.crossfeed.Objects.Comment;
import com.crossfeed.crossfeed.Objects.Photo;
import com.crossfeed.crossfeed.Objects.Post;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by hawk on 17.12.2016.
 */
public class PostViewerActivity extends AppCompatActivity {

    static ArrayList<Photo> post_images = new ArrayList<>();
    ArrayList<FeedImageView> pics = new ArrayList<>();
    LinearLayout image_container;
    Context context;
    LinearLayout.LayoutParams layout_params;
    ImageLoader imageLoader;
    Post post_item;
    VKApi vkApi = new VKApi();
    TextView likes_counter, comments_test;
    ImageButton comment, share, like;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_viewer);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        context = getApplicationContext();
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(R.string.post_view_title);
        Drawable drawable = ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_more_horiz_black_24dp);

        toolbar.setOverflowIcon(drawable);
        toolbar.setNavigationIcon(R.drawable.ic_keyboard_backspace_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        View preLollipopShadow = findViewById(R.id.preLollipopShadow);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            preLollipopShadow.setVisibility(View.VISIBLE);
        }

        post_item = (Post) getIntent().getSerializableExtra("post_item");
        if (post_item != null) {
            //post_item = (Post) getIntent().getSerializableExtra("post_item");
            imageLoader = AppController.getInstance().getImageLoader();

            NetworkImageView parent_icon = (NetworkImageView) findViewById(R.id.post_parent_icon);
            TextView parent_name = (TextView) findViewById(R.id.post_parent_name);
            TextView post_text = (TextView) findViewById(R.id.post_viewer_text);
            image_container = (LinearLayout) findViewById(R.id.feed_image_view_container);


            parent_icon.setImageUrl(post_item.getParentIcon(), imageLoader);
            parent_name.setText(post_item.getParentName());
            post_text.setText(post_item.getPostText());

            TextView time_stamp = (TextView) findViewById(R.id.post_time_stamp);
            CharSequence formatted_time = DateUtils.getRelativeTimeSpanString(
                    post_item.getTimeStamp(), System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);
            time_stamp.setText(formatted_time);

            likes_counter = (TextView) findViewById(R.id.likes_counter);
            likes_counter.setText(post_item.getCountLikes() + " " + getString(R.string.likes_counter_text));

            comments_test = (TextView) findViewById(R.id.comments_test);

            comment = (ImageButton) findViewById(R.id.post_comment_btn);
            share = (ImageButton) findViewById(R.id.post_share_btn);
            like = (ImageButton) findViewById(R.id.post_like_btn);

//            if(post_item.isCanComment()){
//                comment.setVisibility(View.INVISIBLE);
//                comment.setEnabled(false);
//            }

            if (post_item.getIsLiked())
                like.setImageResource(R.drawable.ic_post_like_filled_black_20dp);
            else
                like.setImageResource(R.drawable.ic_post_like_empty_black_20dp);

            comment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (post_item.isCanComment()) {
                        String comments_request = vkApi.getEncodedCommentRequest(post_item.getOwnerId(), post_item.getPostId(), 0, 10);
                        //String comments_request = vkApi.getEncodedCommentRequest(85635407, 3199, 1, 0, 100, "asc", 1);
                        JsonObjectRequest commentsJsonObjRequest = new JsonObjectRequest(Request.Method.POST, comments_request,
                                null, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                ArrayList<Comment> comments = vkApi.parseCommentsJson(response);

                                String txt = "";
                                for (int i = 0; i < comments.size(); i++)
                                    txt += comments.get(i).getAuthorName() + "\n" + comments.get(i).getCommentText() + "\n\n  ";
                                comments_test.setText(txt);

                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(MainActivity.getContext(), "Runtime error: comment request failed", Toast.LENGTH_LONG).show();
                            }
                        });
                        AppController.getInstance().addToRequestQueue(commentsJsonObjRequest);
                    } else
                        Toast.makeText(MainActivity.getContext(), "Comments closed here", Toast.LENGTH_SHORT).show();

                }
            });
            share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
            like.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String like_request = post_item.getAddLikeRequestUrl();
                    String unlike_request = post_item.getDeleteLikeRequestUrl();
                    if (!post_item.getIsLiked()) {
                        post_item.setIsLiked(true);
                        JsonObjectRequest likeJsonObjRequest = new JsonObjectRequest(Request.Method.POST, like_request,
                                null, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                int likes = vkApi.parseLikeRequestResponse(response);
                                post_item.setCountLikes(likes);
                                likes_counter.setText(likes + " " + getString(R.string.likes_counter_text));
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(MainActivity.getContext(), "Runtime error: like request failed", Toast.LENGTH_LONG).show();
                            }
                        });
                        AppController.getInstance().addToRequestQueue(likeJsonObjRequest);
                        like.setImageResource(R.drawable.ic_post_like_filled_black_20dp);
                    } else {
                        post_item.setIsLiked(false);
                        JsonObjectRequest unlikeJsonObjRequest = new JsonObjectRequest(Request.Method.POST, unlike_request, null,
                                new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        int likes = vkApi.parseLikeRequestResponse(response);
                                        post_item.setCountLikes(likes);
                                        likes_counter.setText(likes + " " + getString(R.string.likes_counter_text));
                                    }
                                }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(MainActivity.getContext(), "Runtime error: like request failed", Toast.LENGTH_LONG).show();
                            }
                        });
                        AppController.getInstance().addToRequestQueue(unlikeJsonObjRequest);
                        like.setImageResource(R.drawable.ic_post_like_empty_black_20dp);
                    }
                }
            });

            post_images = post_item.getPostImages();

            layout_params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            layout_params.setMargins(0, 8, 0, 8);

            Thread creating_pics = new Thread() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            for (int i = 0; i < post_images.size(); i++) {
                                FeedImageView feed_image = new FeedImageView(context);
                                feed_image.setLayoutParams(layout_params);
                                feed_image.setImageUrl(post_images.get(i).getPhoto_604(), imageLoader);
                                image_container.addView(feed_image);
                            }
                        }
                    });
                }
            };
            creating_pics.start();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.postview_toolbar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {
            case R.id.action_share:
                break;
            case R.id.action_settings:
                break;

        }
        return super.onOptionsItemSelected(item);
    }
}