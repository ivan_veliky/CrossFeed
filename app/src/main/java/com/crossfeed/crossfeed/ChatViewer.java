package com.crossfeed.crossfeed;

import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.os.SystemClock;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ResponseDelivery;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.crossfeed.crossfeed.Objects.Dialog;
import com.crossfeed.crossfeed.Objects.LongPollServer;
import com.crossfeed.crossfeed.Objects.Message;
import com.crossfeed.crossfeed.Objects.PollUpdate;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Stack;

/**
 * Created by hawk on 09.03.2017.
 */
public class ChatViewer extends AppCompatActivity {
    Stack<Message> messages;
    Stack<Message> loaded_messages;
    //ArrayList<Dialog> dialogs;
    VKApi vkApi;
    ChatListAdapter adapter;
    //DialogsListAdapter adapter;
    ListView chat_list;
    final int DEFAULT = 25;
    boolean flag_loading;
    long user_id=-1;
    ProgressBar progressBar;
    EditText chat_et;
    ImageLoader imageLoader;
    LongPollServer server;
    Thread pollingTask;
    ArrayList<PollUpdate> poll_upds;
    Dialog dialog;
    boolean flag_polling;
    int index=0, top;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_viewer);
        //Adapter
        vkApi = new VKApi();
        flag_polling = true;
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        loaded_messages = messages = new Stack<>();

        View preLollipopShadow = findViewById(R.id.preLollipopShadow);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            preLollipopShadow.setVisibility(View.VISIBLE);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        chat_et = (EditText) findViewById(R.id.chat_edit_text);
        imageLoader = AppController.getInstance().getImageLoader();
        progressBar = (ProgressBar) findViewById(R.id.chat_progress);
        NetworkImageView chat_icon = (NetworkImageView) findViewById(R.id.chat_icon);
        Button send_bt = (Button) findViewById(R.id.chat_send_bt);

        setSupportActionBar(toolbar);
        chat_list = (ListView) findViewById(R.id.messages_list);
        Drawable drawable = ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_more_horiz_black_24dp);

        toolbar.setOverflowIcon(drawable);
        toolbar.setNavigationIcon(R.drawable.ic_keyboard_backspace_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        progressBar.setVisibility(View.VISIBLE);

        chat_list.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (totalItemCount != 0 && totalItemCount - firstVisibleItem == totalItemCount - 2) {
                    if (!flag_loading) {
                        flag_loading = true;
                        load_next_messages();  // lazy load
                    }
                }
            }
        });


        String pollRequest = vkApi.getConnectLongPollServerRequest();
        JsonObjectRequest pollJsonRequest = new JsonObjectRequest(Request.Method.GET, pollRequest,
                null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (response != null)
                    server = vkApi.parseConnectLongPollServerResponse(response);

                pollingTask = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        //Looper.prepare();
                       // if (flag_polling)
                            //pollForMessages();
                        //Looper.loop();
                    }
                });
                pollingTask.start();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MainActivity.getContext(), "error while pollRequest", Toast.LENGTH_SHORT).show();
            }
        });
        AppController.getInstance().addToRequestQueue(pollJsonRequest);


        dialog = (Dialog) getIntent().getSerializableExtra("dialog_item");
        if (dialog != null) {
            if (getSupportActionBar() != null)
                getSupportActionBar().setTitle("");

            TextView chat_title = (TextView) findViewById(R.id.chat_title);
            chat_title.setText(dialog.getTitle());

            final long CHAT_IDENTIFIER = 2000000000;
            user_id = dialog.getIsGroupChat() ? CHAT_IDENTIFIER + dialog.getChatId() : dialog.getId();
            chat_icon.setImageUrl(dialog.getPhoto_50(), imageLoader);
            //dialogs.add(dialog);
            String request = vkApi.getEncodedHistoryRequest(user_id, DEFAULT, dialog.getMId(), 1);
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, request,
                    null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    messages = vkApi.parseChatHistory(response);
                    progressBar.setVisibility(View.GONE);
                    notifyAdapter();

                    ArrayList<Long> mids = getMidMarkReadFrom();
                    String mark_request = vkApi.getEncodedMarkAsReadRequest(mids);
                    JsonObjectRequest jsonMarkAsReadRequest = new JsonObjectRequest(Request.Method.GET, mark_request, null,
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                }
                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(MainActivity.getContext(), "error while reading", Toast.LENGTH_SHORT).show();
                        }
                    });
                    if (mids.size() != 0)
                        AppController.getInstance().addToRequestQueue(jsonMarkAsReadRequest);

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(MainActivity.getContext(), "error while connecting", Toast.LENGTH_SHORT).show();
                }
            });

            AppController.getInstance().addToRequestQueue(jsonObjectRequest);
        }

        send_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage();
            }
        });

        chat_et.setOnEditorActionListener(new TextView.OnEditorActionListener() {   //keyboard action
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    sendMessage();
                    return true;
                }
                return false;
            }
        });

    }

    void sendMessage() {
        String message = chat_et.getText().toString().trim();
        if (message.equals("") || messages==null) {
            return;
        }
        chat_et.setText("");
        addMessageToList(message, 1, true, -1, false);

        String send_request = vkApi.getEncodedSendMessageQuery(user_id, message);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                send_request, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try{
                    long mid = response.getLong("response");
                    Message item = (Message) adapter.getItem(adapter.getCount()-1);
                    item.setMid(mid);
                    item.setSendState(0); // 0 - means sent state
                    item.setDate(SystemClock.currentThreadTimeMillis());
                    adapter.notifyDataSetChanged();
                    //adapter.notifyDataSetInvalidated();
                }
                catch(JSONException e) {
                    Toast.makeText(MainActivity.getContext(), "error while getting mid", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Message item = (Message) adapter.getItem(adapter.getCount()-1);
                item.setSendState(2);   // 2 - means error state
                adapter.notifyDataSetChanged();
                adapter.notifyDataSetInvalidated();
                Toast.makeText(MainActivity.getContext(), "error while sending", Toast.LENGTH_SHORT).show();
            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    void addMessageToList(String text, int send_state, boolean read, long date, boolean out) {
        Message item = new Message();
        item.setBody(text);
        item.setSendState(send_state); // 1 means sending state
        item.setRead(read);
        if(date!=-1)
            item.setDate(date);
        item.setOut(out);

        messages.add(item);
        adapter.notifyDataSetChanged();
        chat_list.setSelection(adapter.getCount()-1);
        dialog.setBody(text);
        dialog.setRead_state(true);
    }

    ArrayList<Long> getMidMarkReadFrom() {
        ArrayList<Long> mids= new ArrayList<>();
        Message item;
        if(messages!=null && messages.size()!=0)
        for(int i=messages.size()-1; i>=0;i--) {
            item = messages.get(i);
            if(item.getRead())
                mids.add(item.getMid());
        }
        return mids;
    }


    void notifyAdapter() {
        adapter = new ChatListAdapter(this, messages);
        chat_list.setAdapter(adapter);
    }

    void load_next_messages() {
        long start_mid = messages.get(0).getMid();
        //Toast.makeText(MainActivity.getContext(), "start_mid="+start_mid, Toast.LENGTH_SHORT).show();

        String request = vkApi.getEncodedHistoryRequest(user_id, DEFAULT, start_mid, 1);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, request,
                null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                loaded_messages = vkApi.parseChatHistory(response);
                addMessages(loaded_messages);

                int index = chat_list.getFirstVisiblePosition()+loaded_messages.size()-2;
                View v = chat_list.getChildAt(0);
                int top = v==null?0:v.getTop();

                adapter.notifyDataSetChanged();
                chat_list.setSelectionFromTop(index, top);
                flag_loading = false;
                //notifyAdapter();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MainActivity.getContext(), "error while connecting", Toast.LENGTH_SHORT).show();
            }
        });

        AppController.getInstance().addToRequestQueue(jsonObjectRequest);

    }

    void addMessages(Stack<Message> new_messages) {
        for(int i=0;i<new_messages.size()-2;i++) {
            messages.add(i, new_messages.get(i));
        }
    }

    public void pollForMessages() {

        flag_polling = false;

        String pollRequest = vkApi.getLongPollServerRequest(server.getKey(),
                server.getServer(), server.getTs(), 25);
        JsonObjectRequest poll_upd_request = new JsonObjectRequest(Request.Method.GET, pollRequest,
                null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (response != null) {
                    poll_upds = vkApi.parseLongPollResponse(response, server);
                    if (poll_upds != null && poll_upds.size() != 0)
                        pollToMessages(poll_upds);
                }
                flag_polling = true;
                //pollForMessages();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MainActivity.getContext(), " poll request error ", Toast.LENGTH_SHORT).show();
                //pollForMessages();
            }
        });
        AppController.getInstance().addToRequestQueue(poll_upd_request);
    }

    public void pollToMessages(ArrayList<PollUpdate> upds) {
        long uid = dialog.getId();
        for(int i=0;i<upds.size();i++) {
            PollUpdate item = upds.get(i);
            if(uid == upds.get(i).getUid()) {
                addMessageToList(item.getText(), 0, false, item.getDate(), false);
            }

        }
    }

    protected void onDestroy() {
        super.onDestroy();
        if(pollingTask!=null)
            pollingTask.interrupt();
    }

    public void chageDataSet(ArrayList<PollUpdate> upds) {
        for(int i=0;i<upds.size();i++) {
            for (int j = 0; j < messages.size(); i++) {
                PollUpdate item = upds.get(i);
                //if(messages.get(j))
            }
        }
    }
}


























