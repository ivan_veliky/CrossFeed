package com.crossfeed.crossfeed.Services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.crossfeed.crossfeed.AppController;
import com.crossfeed.crossfeed.MainActivity;
import com.crossfeed.crossfeed.Objects.Dialog;
import com.crossfeed.crossfeed.Objects.LongPollServer;
import com.crossfeed.crossfeed.Objects.PollUpdate;
import com.crossfeed.crossfeed.VKApi;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by hawk on 17.03.2017.
 */
public class LongPollingService extends Service {
    ArrayList<Dialog> dialogs;
    boolean flag_loading;
    LongPollServer server;
    ArrayList<PollUpdate> poll_upds;
    VKApi vkApi= new VKApi();

    public LongPollingService(ArrayList<Dialog> objects) {
        dialogs = objects;
        vkApi = new VKApi();
        flag_loading = false;
    }

    public LongPollingService() {

    }

    public void onCreate() {
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        if(server==null)
            server = (LongPollServer)intent.getSerializableExtra("LongPollServerExtra");


        new Thread(new Runnable() {
            @Override
            public void run() {
                String pollRequest = vkApi.getLongPollServerRequest(server.getKey(),
                        server.getServer(), server.getTs(), 10);
                JsonObjectRequest poll_upd_request = new JsonObjectRequest(Request.Method.GET, pollRequest,
                        null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if(response!=null) {
                            poll_upds = vkApi.parseLongPollResponse(response, server);
                            if(poll_upds!=null && poll_upds.size()!=0)
                                Toast.makeText(MainActivity.getContext(), poll_upds.get(0).getText()+"",
                                        Toast.LENGTH_SHORT).show();
                            else
                            Toast.makeText(MainActivity.getContext(), "onResponse",
                                    Toast.LENGTH_SHORT).show();
                        }
                        stopSelf();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MainActivity.getContext(), "error in service poll upd request",
                                Toast.LENGTH_SHORT).show();
                        stopSelf();
                    }
                });
                AppController.getInstance().addToRequestQueue(poll_upd_request);

            }
        }).start();
        return super.onStartCommand(intent, flags, startId);
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public boolean getFlag() {
        return flag_loading;
    }
}
