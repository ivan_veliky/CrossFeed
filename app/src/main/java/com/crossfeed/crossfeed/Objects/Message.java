package com.crossfeed.crossfeed.Objects;

/**
 * Created by hawk on 09.03.2017.
 */
public class Message {
    String title, body;
    boolean read, out;
    long date, mid;
    int sendState;

    public void setTitle(String name) {
        title= name;
    }
    public void setBody(String text) {
        body = text;
    }
    public void setRead(boolean flag) {
        read = flag;
    }
    public void setOut(boolean flag) {
        out = flag;
    }
    public void setDate(long time) {
        date = time;
    }

    public String getTitle() {
        return title;
    }
    public String getBody() {
        return body;
    }
    public boolean getRead() {
        return read;
    }
    public boolean getOut() {
        return out;
    }
    public long getDate() {
        return date;
    }
    public void setMid(long id) {
        mid = id;
    }
    public long getMid() {
        return mid;
    }
    public void setSendState(int state) {
        sendState = state;
    }
    public int getSendState() {
        return sendState;
    }
}
