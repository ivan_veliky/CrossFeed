package com.crossfeed.crossfeed;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.crossfeed.crossfeed.Objects.Dialog;

import java.util.List;


/**
 * Created by hawk on 26.02.2017.
 */
public class DialogsListAdapter extends BaseAdapter {
    List<Dialog> dialogs;
    Context context;
    ImageLoader imageLoader;
    LayoutInflater inflater;
    SharedPreferences prefs;

    public DialogsListAdapter(Context context, List<Dialog> dialogs) {
        this.dialogs = dialogs;
        this.context = context;
        imageLoader = AppController.getInstance().getImageLoader();
        this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        prefs = context.getSharedPreferences("profile_info", 0);
    }

    @Override
    public int getCount() {
        return dialogs.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Object getItem(int position) {
        return dialogs.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        TextView title, body, date;
        NetworkImageView icon, out_mark;
        LinearLayout holder;
        Dialog item = (Dialog)getItem(position);

        if(convertView == null) {
            convertView = inflater.inflate(R.layout.dialog_item, parent, false);
            //viewHolder = new ViewHolder();
        }

            title = (TextView) convertView.findViewById(R.id.dialog_name);
            icon = (NetworkImageView) convertView.findViewById(R.id.dialog_icon);
            body = (TextView) convertView.findViewById(R.id.dialog_body);
            out_mark = (NetworkImageView) convertView.findViewById(R.id.answer_out_mark_dialog);
            holder = (LinearLayout) convertView.findViewById(R.id.dialog_item_parent_holder);
            date = (TextView) convertView.findViewById(R.id.dialog_time_stamp);
            //setTag(viewHolder);

        //else {
           // viewHolder = (ViewHolder) convertView.getTag();
       // }

        title.setText(item.getTitle());

        body.setText(item.getBody());
        icon.setImageUrl(item.getPhoto_50(), imageLoader);
        String pic_mark = prefs.getString("profile_pic", null);
        if(pic_mark!=null)
            out_mark.setImageUrl(pic_mark, imageLoader);


        if(item.getOut()) {
            out_mark.setVisibility(View.VISIBLE);
            holder.setBackgroundResource(R.color.textWhite);

            if(item.getRead_State()) {
                body.setBackgroundResource(R.color.dialog_unread_message_bg);
            }
            else {
                body.setBackgroundResource(R.color.textWhite);
            }
        }
        else {
            out_mark.setVisibility(View.GONE);

            if(item.getRead_State()) {
                holder.setBackgroundResource(R.color.dialog_unread_message_bg);
                body.setBackgroundResource(R.color.dialog_unread_message_bg);
            }
            else {
                holder.setBackgroundResource(R.color.textWhite);
                body.setBackgroundResource(R.color.textWhite);
            }
        }

        CharSequence formatted_time = DateUtils.getRelativeTimeSpanString(
                item.getDate(), System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);
        date.setText(formatted_time);

        return convertView;
    }

    static class ViewHolder {

        TextView title, body, date;
        NetworkImageView icon, out_mark;
        LinearLayout holder;
    }

}























