package com.crossfeed.crossfeed;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.crossfeed.crossfeed.Objects.Post;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hawk on 05.02.2017.
 */
public class FeedFragment extends Fragment {

    View view;
    ListView listFeed;
    SwipeRefreshLayout refreshLayout;
    VKApi vkApi;
    ListFeedAdapter adapter;
    List<Post> posts, next_posts;
    boolean flag_loading = false;
    ArrayList<String> filters;
    //android.app.ActionBar actionBar;
    SharedPreferences prefs;
    SharedPreferences.Editor edit;
    int firstVisible = 0;

    //int api_count;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        vkApi = new VKApi();
        //actionBar = MainActivity.getMainActivity().getActionBar();
        setHasOptionsMenu(true);
        prefs = MainActivity.getContext().getSharedPreferences("json_cache_feed", 0);
        //api_count = prefs.getInt("apis_count", 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //getActivity().setTitle("CrossFeed");
        if(view==null) {
            view = inflater.inflate(R.layout.fragment_feed, container, false);
            initialize(view);
        }
        return view;
    }

    void initialize (View view) {
        listFeed = (ListView) view.findViewById(R.id.listFeed);
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP)
            listFeed.setNestedScrollingEnabled(true);

        filters = new ArrayList<>();
        filters.add("post");

        //ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar_main);
        next_posts = posts = new ArrayList<>();
        //listFeed.setEmptyView(findViewById(R.id.error_text));
        refreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.refreshLayout);
        refreshLayout.setVisibility(View.VISIBLE);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                updateFeed();
            }
        });
        refreshLayout.setColorSchemeResources(R.color.colorAccent);

        listFeed.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Serializable post_item = (Serializable) listFeed.getItemAtPosition(position);
                Intent intent = new Intent(MainActivity.getContext(), PostViewerActivity.class);
                intent.putExtra("post_item",post_item);
                startActivity(intent);
            }
        });

        listFeed.setOnScrollListener(new AbsListView.OnScrollListener() {
            //int lastFirstPos = 0;
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {}

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                firstVisible = firstVisibleItem;
                if(totalItemCount != 0 && firstVisibleItem+visibleItemCount == totalItemCount-2) {
                    if(!flag_loading) {
                        flag_loading = true;
                        load_next_feed();    // lazy load
                    }
                }
            }
        });

        getFeedFromCache();

    }

    public void updateFeed() {
        refreshLayout.setRefreshing(true);
        String request_url = vkApi.getEncodedFeedRequestUrl(8, 0, filters, false);
        Cache cache = AppController.getInstance().getRequestQueue().getCache();
        Cache.Entry entry = cache.get(request_url);

        if (entry != null) {  // checking cache
            try {
                String data = new String(entry.data, "UTF-8");
                posts = vkApi.parseFeedJson(new JSONObject(data));
                notifyFeed();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                posts = null;
            }
        } else {  // making request
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                    request_url, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    if (response != null) {
                        edit = prefs.edit();
                        edit.putString("feed_json", response.toString());
                        edit.apply();

                        posts = vkApi.parseFeedJson(response);
                        notifyFeed();
                        refreshLayout.setRefreshing(false);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    VolleyLog.d("TAG", "Error: " + volleyError.getMessage());
                    refreshLayout.setRefreshing(false);
                }
            });

            AppController.getInstance().addToRequestQueue(jsonObjectRequest);
        }
    }

    public void notifyFeed() {
        adapter = new ListFeedAdapter(MainActivity.getContext(), posts);
        listFeed.setAdapter(adapter);
    }

    public void load_next_feed() {
        String request_url = vkApi.getEncodedFeedRequestUrl(8, 0, filters, true);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                request_url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (response != null) {
                    next_posts = vkApi.parseFeedJson(response);
                    addPosts(next_posts);
                    adapter.notifyDataSetChanged();
                    flag_loading = false;
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                VolleyLog.d("TAG", "Error: " + volleyError.getMessage());
                Toast.makeText(MainActivity.getContext(), "error while connecting", Toast.LENGTH_SHORT).show();
            }
        });

        AppController.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    public void addPosts(List<Post> new_posts) {
        for(int i=0; i< new_posts.size(); i++) {
            posts.add(new_posts.get(i));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_refresh:
                updateFeed();
                break;
            case R.id.action_feed_mode:
                Toast.makeText(MainActivity.getContext(), getString(R.string.action_feed_mode), Toast.LENGTH_LONG).show();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    void getFeedFromCache() {
        String feed_response = prefs.getString("feed_json", "");

        if(feed_response.equals("")) { // in case there weren't any cache
            updateFeed();
            return;
        }

        try {
            posts = vkApi.parseFeedJson(new JSONObject(feed_response));
            notifyFeed();
        }
        catch (JSONException e) {
            Toast.makeText(MainActivity.getContext(), "cache feed exception", Toast.LENGTH_LONG).show();
        }
    }
}
