package com.crossfeed.crossfeed;

import android.app.Application;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

import Utils.LruBitmapCache;

/**
 * Created by hawk on 11.12.2016.
 */
public class AppController extends Application {
    private ImageLoader mImageLoader;
    private LruBitmapCache mLruBitmapCache;
    private RequestQueue mRequestQueue;
    private static AppController mInstance;

    public static final String TAG = AppController.class.getSimpleName();

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }

    public static synchronized AppController getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if(mRequestQueue==null)
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        return mRequestQueue;
    }

    public LruBitmapCache getLruBitmapCache() {
        if(mLruBitmapCache==null)
            mLruBitmapCache = new LruBitmapCache();
        return mLruBitmapCache;
    }

    public ImageLoader getImageLoader() {
        getRequestQueue();
        if(mImageLoader == null) {
            getLruBitmapCache();
            mImageLoader = new ImageLoader(this.mRequestQueue, this.mLruBitmapCache);
        }
        return mImageLoader;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(tag.isEmpty()? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void CancelPandingRequests(Object tag) {
        if(mRequestQueue!=null) {
            mRequestQueue.cancelAll(tag);
        }
    }
}
