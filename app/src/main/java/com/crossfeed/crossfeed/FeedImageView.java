package com.crossfeed.crossfeed;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.AbsoluteLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

/**
 * Created by hawk on 13.12.2016.
 */
    public class FeedImageView extends NetworkImageView {

        public FeedImageView(Context context) {
            this(context, null);
        }

        public FeedImageView(Context context, AttributeSet attrs) {
            this(context, attrs, 0);
        }

        public FeedImageView(Context context, AttributeSet attrs, int defStyle) {
            super(context, attrs, defStyle);
        }

        @Override
        public void setImageUrl(String url, ImageLoader loader) {
            super.setImageUrl(url, loader);
        }

        @Override
        public void setImageBitmap(Bitmap bm) {
            if(bm!=null) {
                int pic_width = bm.getWidth();
                int pic_height = bm.getHeight();

                adjustSize(pic_width, pic_height);
            }
            super.setImageBitmap(bm);
        }

        public void adjustSize(int pic_width, int pic_height) {
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) getLayoutParams();
            int needed_height;
            int view_width = getWidth();

            if(pic_height==0 || pic_width==0 || pic_height/pic_width>4)
                return;

            needed_height = view_width*pic_height/pic_width;
            layoutParams.height = needed_height;
            layoutParams.width = view_width;
            setLayoutParams(layoutParams);
        }

    }