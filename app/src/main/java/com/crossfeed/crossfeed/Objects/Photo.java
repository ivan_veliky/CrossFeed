package com.crossfeed.crossfeed.Objects;

import java.io.Serializable;

/**
 * Created by hawk on 10.12.2016.
 */
public class Photo implements Serializable {

    private String photo_130, photo_604;
    private int width, height;

    public Photo() {}

    public Photo (int width, int height, String photo_604) {
        this.width = width;
        this.height =  height;
        this.photo_604 = photo_604;
    }

    public void setPhoto_130(String photo) {
        this.photo_130 = photo;
    }
    public String getPhoto_130() {
        return this.photo_130;
    }

    public void setPhoto_604(String photo) {
        this.photo_604 = photo;
    }
    public String getPhoto_604() {
        return this.photo_604;
    }

    public void setWidth(int width) {
        this.width = width;
    }
    public int getImageWidth() {
        return this.width;
    }

    public void setImageHeight(int height) {
        this.height = height;
    }
    public int getImageHeight() {
        return this.height;
    }

    public int getCorrectHeight(Post post_item) {
         return post_item.getMaxWidth()*height/width;
    }

    public void setAdaptedSize(int viewWidth) {
        height = viewWidth*height/width;
        width = viewWidth;
    }
}
