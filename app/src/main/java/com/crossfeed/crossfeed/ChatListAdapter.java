package com.crossfeed.crossfeed;

import android.content.Context;
import android.text.format.DateUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.crossfeed.crossfeed.Objects.Message;

import java.util.Stack;

/**
 * Created by hawk on 09.03.2017.
 */
public class ChatListAdapter extends BaseAdapter {
    Stack<Message> messages;
    Context context;
    LayoutInflater inflater;

    public ChatListAdapter(Context context, Stack<Message> messages) {
        this.context = context;
        this.messages = messages;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return messages.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Object getItem(int position) {
        return messages.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Message item = (Message)getItem(position);
        if(convertView==null) {
            convertView = inflater.inflate(R.layout.message_item, parent, false);
            //convertView = new View(context);
        }
        TextView body = (TextView) convertView.findViewById(R.id.message_text);
        TextView date = (TextView) convertView.findViewById(R.id.message_time_stamp);
        LinearLayout message_frame = (LinearLayout) convertView.findViewById(R.id.message_frame);
        LinearLayout message_holder = (LinearLayout)convertView.findViewById(R.id.messages_holder);
        Button unchecked_out = (Button)convertView.findViewById(R.id.unchecked_out);
        Button unchecked_in = (Button)convertView.findViewById(R.id.unchecked_in);
        Button state_bt = (Button) convertView.findViewById(R.id.message_state_bt);
        int sendState = item.getSendState();

        body.setText(item.getBody());
        CharSequence formatted_time = DateUtils.getRelativeTimeSpanString(
                item.getDate(), System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);
        date.setText(formatted_time);

        if(item.getOut()) {
            message_holder.setGravity(Gravity.START);
            message_frame.setBackgroundResource(R.drawable.chat_in_message_theme);

            unchecked_in.setVisibility(View.GONE);

        }
        else {
            message_holder.setGravity(Gravity.END);
            message_frame.setBackgroundResource(R.drawable.chat_out_message_theme);

            if(item.getRead()) {
                unchecked_in.setVisibility(View.VISIBLE);
            }
            else {
                unchecked_in.setVisibility(View.GONE);
            }


            /* if(!item.getRead()) {
                unchecked_out.setVisibility(View.VISIBLE);
                unchecked_in.setVisibility(View.GONE);
            }
            else {
                unchecked_out.setVisibility(View.GONE);
                unchecked_in.setVisibility(View.GONE);
            }
            */
        }


        switch(sendState) {
            case 0:     //sent
                date.setVisibility(View.VISIBLE);
                state_bt.setVisibility(View.GONE);
                break;
            case 1:         //sending
                date.setVisibility(View.GONE);
                state_bt.setVisibility(View.VISIBLE);
                state_bt.setBackgroundResource(R.drawable.ic_cached_black_24dp);
                break;
            case 2:     //error
                date.setVisibility(View.GONE);
                state_bt.setVisibility(View.VISIBLE);
                state_bt.setBackgroundResource(R.drawable.ic_indeterminate_check_box_black_24dp);
                break;
            default:
                date.setVisibility(View.GONE);
                state_bt.setVisibility(View.GONE);
        }


        return convertView;
    }


}
