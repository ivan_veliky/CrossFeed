package com.crossfeed.crossfeed;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.DataSetObserver;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.crossfeed.crossfeed.Objects.Dialog;
import com.crossfeed.crossfeed.Objects.LongPollServer;
import com.crossfeed.crossfeed.Services.LongPollingService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hawk on 05.02.2017.
 */
public class MultichatFragment extends Fragment {

    View view;
    ListView dialogs_list;
    List<Dialog> dialogs_objects, next_dialog_objects;
    VKApi vkApi;
    DialogsListAdapter adapter;
    List<Long> ids = new ArrayList<>();
    List<Long> next_ids = new ArrayList<>();
    String requestUsers="";
    JsonObjectRequest jsonObjectRequestUsers;
    AppController appController;
    SwipeRefreshLayout refreshLayout;
    SharedPreferences prefs;
    SharedPreferences.Editor edit;
    LongPollServer longPollServer;
    boolean flag_loading;

    @Override
    public void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        vkApi = new VKApi();
        appController = AppController.getInstance();

        setHasOptionsMenu(true);
        prefs = MainActivity.getContext().getSharedPreferences("json_cache_dialogs", 0);

       /* String pollRequest = vkApi.getConnectLongPollServerRequest();
        JsonObjectRequest pollJsonRequest = new JsonObjectRequest(Request.Method.GET, pollRequest,
                null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if(response!=null)
                    longPollServer = vkApi.parseConnectLongPollServerResponse(response);

                    //if(longPollServer!=null)
                       // MainActivity.getContext().startService(
                         //   new Intent(MainActivity.getContext(), LongPollingService.class).putExtra(
                            //        "LongPollServerExtra", longPollServer));
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MainActivity.getContext(), "error while pollRequest", Toast.LENGTH_SHORT).show();
            }
        });
        appController.addToRequestQueue(pollJsonRequest);
        */
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(view==null) {
            view = inflater.inflate(R.layout.fragment_multichat, container, false);
            initialize(view);
        }
        return view;
    }

    void initialize(View view) {
        dialogs_list = (ListView)view.findViewById(R.id.dialogs_list);

        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP)
            dialogs_list.setNestedScrollingEnabled(true);

        //users_info_tv = (TextView) view.findViewById(R.id.users_info_tv);
        refreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.refreshDialogLayout);
        refreshLayout.setColorSchemeResources(R.color.colorAccent);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                updateDialogs();
            }
        });

        dialogs_objects= new ArrayList<>();
        adapter = new DialogsListAdapter(MainActivity.getContext(), dialogs_objects);

        dialogs_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Serializable dialog_item = (Serializable) dialogs_list.getItemAtPosition(position);
                Intent intent = new Intent(MainActivity.getContext(), ChatViewer.class);
                intent.putExtra("dialog_item", dialog_item);
                intent.putExtra("long_poll_server", longPollServer);
                startActivity(intent);
            }
        });

        dialogs_list.setOnScrollListener(new AbsListView.OnScrollListener() {
            //int lastFirstPos = 0;
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {}

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if(totalItemCount != 0 && firstVisibleItem+visibleItemCount == totalItemCount-1) {
                    if(!flag_loading) {
                        flag_loading = true;
                        load_next_dialogs();    // lazy load
                    }
                }
            }
        });

        getDialogsFromCache();
    }

    void updateDialogs() {
        refreshLayout.setRefreshing(true);
        //flag_loading = true;
        String requestDialogs = vkApi.getEncodedDialogsRequestUrl(0);
        //Toast.makeText(MainActivity.getContext(), requestUsers, Toast.LENGTH_SHORT).show();

        JsonObjectRequest jsonObjectRequestDialogs = new JsonObjectRequest(Request.Method.GET, requestDialogs,
                null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (response != null) {
                    edit = prefs.edit();
                    edit.putString("dialogs_json", response.toString()); // preference cache
                    edit.apply();

                    dialogs_objects = vkApi.parseDialogsGetJson(response);
                    for(int i=0;i<dialogs_objects.size();i++) {
                        Dialog dialog = dialogs_objects.get(i);
                        if(!dialog.getIsGroupChat())
                            ids.add(dialog.getId());
                    }
                    requestUsers = vkApi.getUsersRequestUrl(ids);
                    jsonObjectRequestUsers = new JsonObjectRequest(Request.Method.GET, requestUsers,
                            null, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            if(response!=null) {
                                edit = prefs.edit();
                                edit.putString("users_json", response.toString()); // preference cache
                                edit.apply();

                                vkApi.parseUsersJson(response, dialogs_objects);
                                notifyAdapter();
                                refreshLayout.setRefreshing(false);
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            VolleyLog.d("TAG", "Error: " + error.getMessage());
                            Toast.makeText(MainActivity.getContext(), "error while connecting to users", Toast.LENGTH_SHORT).show();
                            refreshLayout.setRefreshing(false);
                        }
                    });
                   // Toast.makeText(MainActivity.getContext(), requestUsers, Toast.LENGTH_SHORT).show();
                    appController.addToRequestQueue(jsonObjectRequestUsers);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("TAG", "Error: " + error.getMessage());
                Toast.makeText(MainActivity.getContext(), "error while connecting", Toast.LENGTH_SHORT).show();
                refreshLayout.setRefreshing(false);
            }
        });

        appController.addToRequestQueue(jsonObjectRequestDialogs);

        //appController.addToRequestQueue(jsonObjectRequestUsers);
    }

    void notifyAdapter() {
        adapter = new DialogsListAdapter(MainActivity.getContext(), dialogs_objects);
        dialogs_list.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_refresh:
                updateDialogs();
                break;
            case R.id.action_feed_mode:
                Toast.makeText(MainActivity.getContext(), getString(R.string.action_feed_mode), Toast.LENGTH_LONG).show();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    void getDialogsFromCache() {
        String dialogs_response = prefs.getString("dialogs_json", ""),
                users_response = prefs.getString("users_json", "");

        if(dialogs_response.equals("") || users_response.equals("")) { // in case there weren't any cache json
            updateDialogs();
            return;
        }

        try {
            dialogs_objects = vkApi.parseDialogsGetJson(new JSONObject(dialogs_response));
            vkApi.parseUsersJson(new JSONObject(users_response), dialogs_objects);
            notifyAdapter();

        }
        catch(JSONException e) {
            Toast.makeText(MainActivity.getContext(), "json_cache exception", Toast.LENGTH_SHORT).show();
        }
    }

    void load_next_dialogs() {
        long last_mid = dialogs_objects.get(dialogs_objects.size()-1).getMId();
        String requestDialogs = vkApi.getEncodedDialogsRequestUrl(last_mid);

        JsonObjectRequest jsonObjectRequestDialogs = new JsonObjectRequest(Request.Method.GET, requestDialogs,
                null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (response != null) {
                    next_dialog_objects = vkApi.parseDialogsGetJson(response);
                    for(int i=0;i<next_dialog_objects.size();i++) {
                        Dialog dialog = next_dialog_objects.get(i);
                        if(!dialog.getIsGroupChat())
                            next_ids.add(dialog.getId());
                    }
                    requestUsers = vkApi.getUsersRequestUrl(next_ids);
                    jsonObjectRequestUsers = new JsonObjectRequest(Request.Method.GET, requestUsers,
                            null, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            if(response!=null) {
                                vkApi.parseUsersJson(response, next_dialog_objects);
                                addDialogs(next_dialog_objects);
                                adapter.notifyDataSetChanged();
                                flag_loading = false;
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            VolleyLog.d("TAG", "Error: " + error.getMessage());
                            Toast.makeText(MainActivity.getContext(), "error while connecting to users", Toast.LENGTH_SHORT).show();
                        }
                    });
                    // Toast.makeText(MainActivity.getContext(), requestUsers, Toast.LENGTH_SHORT).show();
                    appController.addToRequestQueue(jsonObjectRequestUsers);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("TAG", "Error: " + error.getMessage());
                Toast.makeText(MainActivity.getContext(), "error while connecting", Toast.LENGTH_SHORT).show();
                //refreshLayout.setRefreshing(false);
            }
        });

        //if(next_dialog_objects!=null && next_dialog_objects.size()!=1)
            appController.addToRequestQueue(jsonObjectRequestDialogs);

    }

    void addDialogs(List<Dialog> new_dialogs) {
        for(int i=1;i<new_dialogs.size();i++) {
            dialogs_objects.add(new_dialogs.get(i));
        }
    }

}
