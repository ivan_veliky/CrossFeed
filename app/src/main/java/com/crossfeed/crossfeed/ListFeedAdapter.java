package com.crossfeed.crossfeed;

import android.content.Context;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.crossfeed.crossfeed.Objects.Photo;
import com.crossfeed.crossfeed.Objects.Post;

import org.json.JSONObject;

import java.util.List;

import static com.crossfeed.crossfeed.R.id.likes_counter;
import static com.crossfeed.crossfeed.R.id.view_counter;
import static com.crossfeed.crossfeed.R.layout.post_item;

/**
 * Created by hawk on 30.11.2016.
 */
public class ListFeedAdapter extends BaseAdapter {
    Context context;
    LayoutInflater inflater;
    List<Post> posts;
    NetworkImageView post_image;
    ImageLoader imageLoader;
    VKApi vkApi;
    Post post;
    ViewHolder viewHolder;

    public ListFeedAdapter(Context context, List<Post> posts) {
        this.context = context;
        this.posts = posts;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        imageLoader = AppController.getInstance().getImageLoader();
        vkApi = new VKApi();
    }

    @Override
    public int getCount() {
        return posts.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Object getItem(int position) {
        return posts.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        post = (Post)getItem(position);

        if(convertView==null) {
            convertView = inflater.inflate(post_item, parent, false);
            viewHolder = new ViewHolder();

            viewHolder.post_text = (TextView) convertView.findViewById(R.id.post_text);
            viewHolder.parent_name = (TextView) convertView.findViewById(R.id.post_parent_name);
            viewHolder.post_time_stamp = (TextView) convertView.findViewById(R.id.post_time_stamp);
            viewHolder.parent_icon = (NetworkImageView) convertView.findViewById(R.id.post_parent_icon);
            viewHolder.post_image = (FeedImageView) convertView.findViewById(R.id.feed_image);

            viewHolder.likes_counter = (TextView)convertView.findViewById(likes_counter);
            viewHolder.likes_counter.setText("" + post.getCountLikes());

            viewHolder.view_counter = (TextView)convertView.findViewById(view_counter);
            viewHolder.view_counter.setText("" + post.getViews());

            viewHolder.share = (ImageButton)convertView.findViewById(R.id.post_share_btn);
            viewHolder.like = (ImageButton)convertView.findViewById(R.id.post_like_btn);
            if(post.getIsLiked()){
                viewHolder.like.setImageResource(R.drawable.ic_post_like_filled_black_20dp);
            }

            viewHolder.share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

            convertView.setTag(viewHolder);
        }
        else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if(imageLoader==null)
            imageLoader = AppController.getInstance().getImageLoader();

        if(post.getPostImages().size()!=0) {
            Photo photo = post.getPostImages().get(0);
            String pic = photo.getPhoto_604();
            LinearLayout.LayoutParams layout_params = new LinearLayout.LayoutParams(
                    post.getMaxWidth(), photo.getCorrectHeight(post));

            viewHolder.post_image.setLayoutParams(layout_params);
            viewHolder.post_image.setImageUrl(pic, imageLoader);
            viewHolder.post_image.setVisibility(View.VISIBLE);
        }
        else
            viewHolder.post_image.setVisibility(View.GONE);

        viewHolder.like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String like_request = post.getAddLikeRequestUrl();
                String unlike_request = post.getDeleteLikeRequestUrl();
                if(!post.getIsLiked()){
                    post.setIsLiked(true);
                    JsonObjectRequest likeJsonObjRequest = new JsonObjectRequest(Request.Method.POST, like_request,
                            null, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            int likes = vkApi.parseLikeRequestResponse(response);
                            viewHolder.likes_counter.setText("" + likes);
                            post.setCountLikes(likes);
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(MainActivity.getContext(), "Runtime error: like request failed", Toast.LENGTH_LONG).show();
                        }
                    });
                    AppController.getInstance().addToRequestQueue(likeJsonObjRequest);
                    viewHolder.like.setImageResource(R.drawable.ic_post_like_filled_black_20dp);
                }else{
                    post.setIsLiked(false);
                    JsonObjectRequest unlikeJsonObjRequest = new JsonObjectRequest(Request.Method.POST, unlike_request,
                            null, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            int likes = vkApi.parseLikeRequestResponse(response);
                            viewHolder.likes_counter.setText("" + likes);
                            post.setCountLikes(likes);
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(MainActivity.getContext(), "Runtime error: like request failed", Toast.LENGTH_LONG).show();
                        }
                    });
                    AppController.getInstance().addToRequestQueue(unlikeJsonObjRequest);
                    viewHolder.like.setImageResource(R.drawable.ic_post_like_empty_black_20dp);
                }
            }
        });

        String cropped_text = post.getCroppedText();
        String name = post.getParentName();

        viewHolder.parent_name.setText(name);

        viewHolder.parent_icon.setImageUrl(post.getParentIcon(), imageLoader);

        viewHolder.post_text.setText(cropped_text);
        viewHolder.post_text.setVisibility(View.VISIBLE);

        CharSequence formatted_time = DateUtils.getRelativeTimeSpanString(
                post.getTimeStamp(), System.currentTimeMillis(),DateUtils.SECOND_IN_MILLIS);
        viewHolder.post_time_stamp.setText(formatted_time);



        return convertView;
    }


    static class ViewHolder {
        TextView parent_name, post_text, post_time_stamp, likes_counter, view_counter;
        NetworkImageView parent_icon;
        FeedImageView post_image;
        ImageButton share, like;

    }

}












