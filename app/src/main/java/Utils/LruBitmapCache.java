package Utils;

import android.graphics.Bitmap;
import android.support.v4.util.LruCache;

import com.android.volley.toolbox.ImageLoader;


/**
 * Created by hawk on 11.12.2016.
 */
public class LruBitmapCache extends android.util.LruCache<String, Bitmap> implements
        ImageLoader.ImageCache {

    public static int getDefaultLruSize() {
        final int max_mem = (int)(Runtime.getRuntime().maxMemory()/1024);
        final int cache_mem = max_mem/4;
        return cache_mem;
    }

    public LruBitmapCache () {
        super(getDefaultLruSize());
    }
    public LruBitmapCache (int size_in_kg) {
        super(size_in_kg);
    }

    @Override
    protected int sizeOf(String key, Bitmap bm) {
        return bm.getByteCount()/1024;
    }

    @Override
    public Bitmap getBitmap(String url) {
        return get(url);
    }

    @Override
    public void putBitmap(String url, Bitmap bm) {
        put(url, bm);
    }


}
