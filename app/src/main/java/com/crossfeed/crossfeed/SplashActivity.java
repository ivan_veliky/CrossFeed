package com.crossfeed.crossfeed;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

/**
 * Created by Roman on 23.11.2016.
 */

public class SplashActivity extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        Intent start_app = new Intent(this, com.crossfeed.crossfeed.MainActivity.class);
        startActivity(start_app);
        finish();
    }
}
