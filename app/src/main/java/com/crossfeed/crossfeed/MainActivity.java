package com.crossfeed.crossfeed;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.crossfeed.crossfeed.Auth.AuthActivity;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    static Context context;
    static Toolbar toolbar;
    static Activity mainActivity;
    DrawerLayout drawer;
    NavigationView navigationView;
    VKApi vkApi;
    SharedPreferences prefs;

    //Menu menu;
    //int api_count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getApplicationContext();
        mainActivity = this;

        vkApi = new VKApi();
        prefs = getSharedPreferences("profile_info", 0);
        String profile_pic = prefs.getString("profile_pic", "");
        String request_profile = vkApi.getUsersRequestUrl(null);
        if(profile_pic.equals("")) {
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                    request_profile, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    if (response != null) {
                        vkApi.setPrefsProfileInfo(response, prefs);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    VolleyLog.d("TAG", "Error: " + volleyError.getMessage());
                    Toast.makeText(MainActivity.getContext(), "error while receiving profile", Toast.LENGTH_SHORT).show();
                }
            });
            AppController.getInstance().addToRequestQueue(jsonObjectRequest);
        }


        //api_count = prefs.getInt("apis_count", 0);
        int icons_id[] = {R.drawable.ic_email_black_24dp, R.drawable.ic_whatshot_black_24dp};
        //TextView network_access_error_tv = (TextView)findViewById(R.id.network_access_error_tv);

        if (!ApiController.isNetworkConnected()) {
            setContentView(R.layout.activity_network_error);
        }
        if (!vkApi.getAuthState()) {
            setContentView(R.layout.empty);

            ImageView plus_img = (ImageView) findViewById(R.id.plus);
            plus_img.setImageResource(R.drawable.plus_blue);
            plus_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, AuthActivity.class);
                    startActivity(intent);
                }
            });

        } else {
            setContentView(R.layout.activity_main);
            //network_access_error_tv.setVisibility(View.GONE);

            toolbar = (Toolbar) findViewById(R.id.toolbar);
            toolbar.setTitle("MultiChat");
            setSupportActionBar(toolbar);

            drawer = (DrawerLayout) findViewById(R.id.nav_drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.addDrawerListener(toggle);
            drawer.addDrawerListener(new DrawerLayout.DrawerListener() {
                @Override
                public void onDrawerSlide(View drawerView, float slideOffset) {}
                @Override
                public void onDrawerOpened(View drawerView) {}
                @Override
                public void onDrawerClosed(View drawerView) {}

                @Override
                public void onDrawerStateChanged(int newState) {
                    navigationView.getMenu().getItem(0).setChecked(true);
                }
            });
            toggle.syncState();

            navigationView = (NavigationView)findViewById(R.id.nav_drawer);
            navigationView.setNavigationItemSelectedListener(this);

            //navigationView.getMenu().getItem(0).setChecked(true);

            ViewPager pager = (ViewPager) findViewById(R.id.pager);
            ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
            pager.setAdapter(adapter);
            pager.setCurrentItem(0); // setting default tab

            pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                String titles[] = {"MultiChat", "CrossFeed"};

                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    toolbar.setTitle(titles[position]);
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });

            TabLayout tabs = (TabLayout) findViewById(R.id.tabs);
            if (tabs != null) {
                tabs.setupWithViewPager(pager);
                for (int i = 0; i < tabs.getTabCount(); i++) {
                    TabLayout.Tab tab = tabs.getTabAt(i);
                    if (tab != null)
                        tab.setIcon(icons_id[i]);
                }
            }

            View preLollipopShadow = findViewById(R.id.preLollipopShadow);
            if(Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                preLollipopShadow.setVisibility(View.VISIBLE);
                if(tabs!=null) {
                    //tabs.setLayoutParams(new LinearLayout.LayoutParams(
                           // LinearLayout.LayoutParams.MATCH_PARENT, 5));
                    tabs.setVisibility(View.GONE);

                }
            }
            else {
                AppBarLayout.LayoutParams appbarParams = (AppBarLayout.LayoutParams)toolbar.getLayoutParams();
                appbarParams.setScrollFlags(AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL
                        | AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS );
            }

            Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.ic_more_horiz_black_24dp);
            toolbar.setOverflowIcon(drawable);
        }
    }

    @Override
    public void onBackPressed(){
        DrawerLayout drawer = (DrawerLayout)findViewById(R.id.nav_drawer_layout);
        if(drawer.isDrawerOpen(GravityCompat.START)){
            drawer.closeDrawer(GravityCompat.START);
        }else{
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu m) {
        //menu = m;
        getMenuInflater().inflate(R.menu.main_toolbar_menu, m);
        return true;
    }

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_refresh:
                //updateFeed();
                break;
            case R.id.action_feed_mode:
                Toast.makeText(MainActivity.this, getString(R.string.action_feed_mode), Toast.LENGTH_LONG).show();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    */



    @Override
    public boolean onNavigationItemSelected(MenuItem item){
        int id = item.getItemId();

        switch (id){
            case R.id.nav_action_home:
                break;
            case R.id.nav_action_friends:
                Toast.makeText(MainActivity.this, getString(R.string.action_friends), Toast.LENGTH_LONG).show();
                break;
            case R.id.nav_action_groups:
                Toast.makeText(MainActivity.this, getString(R.string.action_groups), Toast.LENGTH_LONG).show();
                break;
            case R.id.nav_action_music:
                Toast.makeText(MainActivity.this, getString(R.string.action_music), Toast.LENGTH_LONG).show();
                break;
            case R.id.nav_action_auth:
                Intent intent = new Intent(this, AuthActivity.class);
                startActivity(intent);
                //finish();
                break;
            case R.id.nav_action_settings:
                Toast.makeText(MainActivity.this, getString(R.string.action_settings), Toast.LENGTH_LONG).show();
                break;
        }
        //navigationView.getMenu().getItem(0).setChecked(true);
        DrawerLayout drawer = (DrawerLayout)findViewById(R.id.nav_drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public static synchronized Context getContext() {
        return context;
    }

    public static synchronized Activity getMainActivity() {
        return mainActivity;
    }

    public static int getDisplayWidth() {
        WindowManager windowManager = (WindowManager)
                MainActivity.getContext().getSystemService(Context.WINDOW_SERVICE);

        return windowManager.getDefaultDisplay().getWidth();
    }
}


