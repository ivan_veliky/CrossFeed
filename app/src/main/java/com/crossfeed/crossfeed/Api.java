package com.crossfeed.crossfeed;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.crossfeed.crossfeed.Objects.Post;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by hawk on 28.11.2016.
 */
public abstract class Api {

    private String auth_url,client_id,request_url,
            scope,redirect_uri,access_token,NetName, secret;

    protected String display = "mobile";
    protected String response_type = "token";
    protected String v = "5.60";
    protected String not_found_pic_url = "https://eservices.isca.org.sg/resource/1471202925000/ISCNimages/imgNotfound.jpg";

    SharedPreferences prefs;
    SharedPreferences.Editor editor;

    public Api () {
    }

    protected  void setPrimaryFields(String auth_url, String client_id, String request_url,
                                     String scope, String redirect_uri, String NetName, String secret) {
        this.auth_url = auth_url;
        this.client_id = client_id;
        this.request_url = request_url;
        this.scope = scope;
        this.redirect_uri = redirect_uri;
        this.NetName = NetName;
        this.secret = secret;
    }

    public abstract String getEncodedAuthUrl();
    public abstract String getEncodedMessagesRequestUrl(int out, int count);
    public abstract ArrayList<String> getMessages();
    public abstract ArrayList<Post> parseFeedJson(JSONObject item);
    public abstract String getEncodedFeedRequestUrl(int count, int return_banned,
                                                    ArrayList<String> filters, boolean next_loading_flag);


    public String getNetName() {
        return NetName;
    }
    public String getAuthURL() {
        return auth_url;
    }
    public String getRequestUrl() {
        return request_url;
    }
    public void makeText(Context context) {
        Toast.makeText(context,getNetName()+" is connected", Toast.LENGTH_SHORT).show();
    }


    public String getAccessToken() {
        prefs = MainActivity.getContext().getSharedPreferences("auth_init", 0);
        editor = prefs.edit();
        access_token = prefs.getString(getNetName()+"Token", "");
        editor.apply();
        return access_token;
    }

    public boolean getAuthState() {
        prefs = MainActivity.getContext().getSharedPreferences("auth_init", 0);
        return prefs.getBoolean(getNetName()+"AuthState", false);
    }

    public String getResponseText(String urlText) {
        StringBuilder response = new StringBuilder();
        try {
            URL url = new URL(urlText);
            HttpURLConnection conn = (HttpURLConnection)url.openConnection();

            if(conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String current_line ;
                while((current_line = reader.readLine())!= null) {
                    response.append(current_line);
                }
                reader.close();
            }
            else
                response.append("| response code is invalid");
        }
        catch(MalformedURLException e) {
            response.append("| invalid url exception was thrown");
        }
        catch(IOException e) {
            response.append("| error while getting the response");
        }
        return  response.toString();
    }


    /*
    public Bitmap downloadImage(String url_img) {
        Bitmap img = null;
        InputStream is = null;
        try {
            URL url = new URL(url_img);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                is = conn.getInputStream();
            }
            if (is != null)
                img = BitmapFactory.decodeStream(is);
        } catch (IOException e) {
            img = downloadImage(not_found_pic_url); // initializing trouble_image later
        }
        return img;
    }
    */
}






















