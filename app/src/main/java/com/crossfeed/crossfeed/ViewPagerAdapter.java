package com.crossfeed.crossfeed;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

/**
 * Created by hawk on 05.02.2017.
 */
   class ViewPagerAdapter extends FragmentPagerAdapter {
    ArrayList<String> page_titles = new ArrayList<>();

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
        page_titles.add("MultiChat");
        page_titles.add("CrossFeed");
    }

    @Override
    public Fragment getItem(int position) {
        switch(position) {
            case 0:
                return new MultichatFragment();
            case 1:
                return new FeedFragment();
            default:
                return new MultichatFragment();
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch(position) {
            case 0:
                return page_titles.get(0);
            case 1:
                return page_titles.get(1);
            default:
                return page_titles.get(0);
        }

    }
}
