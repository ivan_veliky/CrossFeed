package com.crossfeed.crossfeed.Auth;

/**
 * Created by hawk on 15.11.2016.
 */

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.crossfeed.crossfeed.Api;
import com.crossfeed.crossfeed.ApiController;
import com.crossfeed.crossfeed.MainActivity;
import com.crossfeed.crossfeed.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;


public class AuthActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    //Toolbar toolbar;
    //ListView auth_list;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    private static Context context;
    //ArrayList<Map<String, Object>> auth_nets_list;
    //SimpleAdapter adapter;
    //final String ATTRIBUTE_NAME = "name";
    //final String ATTRIBUTE_IMAGE = "image";
    //final String ATTRIBUTE_STATE = "state";
    TextView net_switch_name;

    //String[] net_names = {"VK", "Twitter"};
   // int[] net_images = {R.drawable.vk200, R.drawable.twitter};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final String ATTRIBUTE_NAME = "name";
        final String ATTRIBUTE_IMAGE = "image";
        final String ATTRIBUTE_STATE = "state";
        SimpleAdapter adapter;
        ArrayList<Map<String, Object>> auth_nets_list;
        Toolbar toolbar;
        ListView auth_list;
        List<String> net_names = Arrays.asList("VK", "Twitter");
        List<Integer> net_images = Arrays.asList(R.drawable.vk200, R.drawable.twitter);



        if (!ApiController.isNetworkConnected()) {
            setContentView(R.layout.activity_network_error);
        } else {
            setContentView(R.layout.activity_auth);

            context = getApplicationContext();

            toolbar = (Toolbar) findViewById(R.id.auth_tb);
            toolbar.setTitle(R.string.auth_title);
            setSupportActionBar(toolbar);

            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.nav_drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.addDrawerListener(toggle);
            toggle.syncState();

            NavigationView navigationView = (NavigationView) findViewById(R.id.nav_drawer);
            navigationView.setNavigationItemSelectedListener(this);

            navigationView.getMenu().getItem(4).setChecked(true);

            auth_list = (ListView) findViewById(R.id.auth_list);

            prefs = MainActivity.getContext().getSharedPreferences("auth_init", 0);

            auth_nets_list = new ArrayList<>();

            Map<String, Object> net_item;
            for (int i = 0; i < net_names.size(); i++) {
                net_item = new HashMap<>(net_names.size());

                net_item.put(ATTRIBUTE_NAME, net_names.get(i));
                net_item.put(ATTRIBUTE_IMAGE, net_images.get(i));
                net_item.put(ATTRIBUTE_STATE, getResources().getDrawable(R.drawable.plus_icon));

                auth_nets_list.add(i, net_item);
            }

            String[] from = {ATTRIBUTE_NAME, ATTRIBUTE_IMAGE, ATTRIBUTE_STATE};
            int[] to = {R.id.net_name, R.id.net_pic_id, R.id.state_pic};

            adapter = new SimpleAdapter(this, auth_nets_list, R.layout.single_item_net, from, to);
            adapter.setViewBinder(new AuthViewBinder());
            auth_list.setAdapter(adapter);

            auth_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    net_switch_name = (TextView) view.findViewById(R.id.net_name);
                    Api api = ApiController.getApiByName(net_switch_name.getText().toString());

                    if (api != null)
                        if (!api.getAuthState())
                            setConnection(api);
                        else {
                            editor = prefs.edit();
                            editor.putBoolean(api.getNetName() + "AuthState", false);
                            int net_count = prefs.getInt("apis_count", 1);
                            net_count = net_count - 1;
                            editor.putInt("apis_count", net_count);
                            editor.apply();

                            ImageView state_pic = (ImageView) view.findViewById(R.id.state_pic);
                            state_pic.setImageDrawable(getResources().getDrawable(R.drawable.plus_icon));
                        }

                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout)findViewById(R.id.nav_drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.nav_action_home:
                finish();
                break;
            case R.id.nav_action_friends:
                Toast.makeText(AuthActivity.this, getString(R.string.action_friends), Toast.LENGTH_LONG).show();
                break;
            case R.id.nav_action_groups:
                Toast.makeText(AuthActivity.this, getString(R.string.action_groups), Toast.LENGTH_LONG).show();
                break;
            case R.id.nav_action_music:
                Toast.makeText(AuthActivity.this, getString(R.string.action_music), Toast.LENGTH_LONG).show();
                break;
            case R.id.nav_action_auth:
                break;
            case R.id.nav_action_settings:
                Toast.makeText(AuthActivity.this, getString(R.string.action_settings), Toast.LENGTH_LONG).show();
                break;
        }
        DrawerLayout drawer = (DrawerLayout)findViewById(R.id.nav_drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    public void setConnection(Api api) {
        if (api != null) {
            Bundle bundle = new Bundle();
            bundle.putString("url", api.getEncodedAuthUrl());
            bundle.putString("netName", api.getNetName());
            Intent intent = new Intent(getContext(), WebAuthActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }

    public static Context getContext() {
        return context;
    }

}
