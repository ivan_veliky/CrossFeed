package com.crossfeed.crossfeed.Auth;

/**
 * Created by Roman on 15.11.2016.
 */


import android.view.View;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.crossfeed.crossfeed.Api;
import com.crossfeed.crossfeed.ApiController;
import com.crossfeed.crossfeed.R;
import com.crossfeed.crossfeed.VKApi;


public class AuthViewBinder implements SimpleAdapter.ViewBinder {

    ImageView state_pic;
    TextView net_name;
    Api api;
    @Override
    public boolean setViewValue(View view, Object data, String textRepresentation)
    {
        switch (view.getId())
        {
            case R.id.net_name:
                net_name = (TextView) view.findViewById(R.id.net_name);
                break;
            case R.id.state_pic:
                api = ApiController.getApiByName(net_name.getText().toString());
                if(api!=null)
                    if(api.getAuthState()) {
                        set_checked_pic(view);
                    }
                    else {
                        set_unchecked_pic(view);
                    }
                return true;
        }
        return false;
    }

    public void set_checked_pic(View view) {
        state_pic = (ImageView) view;
        state_pic.setImageResource(R.drawable.check);
    }

    public void set_unchecked_pic(View view){
        state_pic = (ImageView) view;
        state_pic.setImageResource(R.drawable.plus_icon);
    }
}
